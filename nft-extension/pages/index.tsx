import { useState, useEffect } from "react";

interface NFTData {
  name: string;
  image_url: string;
  description: string;
  owner: {
    address: string;
  };
  sell_orders?: {
    current_price: string;
  }[];
}

function NFTDetails({ tokenId }: { tokenId: string }) {
  const [nftData, setNftData] = useState<NFTData | null>(null);

  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        `https://api.opensea.io/api/v1/assets?token_ids=${tokenId}`
      );
      const data = await response.json();
      setNftData(data.assets[0]);
    }

    fetchData();
  }, [tokenId]);

  return (
    <div className="my-8 p-4 bg-white rounded-md shadow-md w-auto min-h-screen">
      {nftData ? (
        <div>
          <h2 className="text-2xl font-bold">{nftData.name}</h2>
          <img
            className="my-4 rounded-md"
            src={nftData.image_url}
            alt={nftData.name}
          />
          <p className="text-gray-500">{nftData.description}</p>
          {nftData.owner ? (
            <p className="text-gray-500">Owner: {nftData.owner.address}</p>
          ) : (
            <p className="text-gray-500">Owner information not available</p>
          )}

          <p className="text-gray-500">
            Current Price:{" "}
            {nftData.sell_orders
              ? nftData.sell_orders[0].current_price
              : "Not for sale"}
          </p>
        </div>
      ) : (
        <p>Loading NFT details...</p>
      )}
    </div>
  );
}

function HomePage() {
  const [tokenId, setTokenId] = useState<string>("");

  function handleTokenIdChange(event: React.ChangeEvent<HTMLInputElement>) {
    setTokenId(event.target.value);
  }

  return (
    <div className="container mx-auto w-[400px] p-4">
      <h1 className="text-3xl font-bold mb-8">My NFTs</h1>
      <label htmlFor="tokenIdInput" className="text-lg font-medium">
        Enter NFT Token ID:
      </label>
      <input
        id="tokenIdInput"
        type="text"
        value={tokenId}
        onChange={handleTokenIdChange}
        className="block w-full border border-gray-400 rounded-md p-2 mt-2 mb-4"
      />
      {tokenId && <NFTDetails tokenId={tokenId} />}
    </div>
  );
}

export default HomePage;
